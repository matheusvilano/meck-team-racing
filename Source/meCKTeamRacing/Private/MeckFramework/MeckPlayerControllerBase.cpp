// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#include "MeckFramework/MeckPlayerControllerBase.h"

#include "Delegates/DelegateInstancesImpl.h"
#include "Engine/InputDelegateBinding.h"
#include "GameFramework/PlayerController.h"
#include "MeckFramework/MeckVehiclePawnBase.h"
#include "WheeledVehicleMovementComponent.h"

// Default constructor for this PlayerController.
AMeckPlayerControllerBase::AMeckPlayerControllerBase(){}

// Set up custom input component and bindings.
void AMeckPlayerControllerBase::SetupInputComponent()
{
    Super::SetupInputComponent();

    // Make sure the input component doesn't already exist.
    if (!this->MeckInputComponent)
    {
        // Initialize.
        this->MeckInputComponent = NewObject<UMeckInputComponent>(this);
        this->MeckInputComponent->RegisterComponent();
        this->MeckInputComponent->bBlockInput = false;
        this->MeckInputComponent->Priority = 0;

        // Set up delegates.
        if (UInputDelegateBinding::SupportsInputDelegate(this->GetClass()))
        {
            UInputDelegateBinding::BindInputDelegates(this->GetClass(), this->MeckInputComponent);
        }

        // Push onto the stack.
        this->APlayerController::PushInputComponent(this->MeckInputComponent);
    }
}

// Runs everytime the player controller possesses a pawn. Use to set up input bindings.
void AMeckPlayerControllerBase::OnPossess(APawn* InPawn)
{
    Super::OnPossess(InPawn);

    AMeckVehiclePawnBase* PawnAsVehicle = Cast<AMeckVehiclePawnBase>(InPawn);

    // Bindings for the Vehicle class (and its children).
    if (PawnAsVehicle)
    {
        // Axes
        MeckInputComponent->BindAxis("MoveFrontBack", PawnAsVehicle->GetVehicleMovementComponent(), &UWheeledVehicleMovementComponent::SetThrottleInput);
        MeckInputComponent->BindAxis("MoveRightLeft", PawnAsVehicle->GetVehicleMovementComponent(), &UWheeledVehicleMovementComponent::SetSteeringInput);
        
        // Actions
        MeckInputComponent->BindAction("Handbrake", EInputEvent::IE_Pressed, PawnAsVehicle->GetVehicleMovementComponent(), [PawnAsVehicle](){PawnAsVehicle->GetVehicleMovementComponent()->SetHandbrakeInput(true);});
        MeckInputComponent->BindAction("Handbrake", EInputEvent::IE_Released, PawnAsVehicle->GetVehicleMovementComponent(), [PawnAsVehicle](){PawnAsVehicle->GetVehicleMovementComponent()->SetHandbrakeInput(false);});
    }
}