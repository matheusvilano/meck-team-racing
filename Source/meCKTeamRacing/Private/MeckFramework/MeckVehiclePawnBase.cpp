// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#include "MeckFramework/MeckVehiclePawnBase.h"

// Parameterless constructor.
AMeckVehiclePawnBase::AMeckVehiclePawnBase()
{
    // Camera
    {
        // Spring Arm Initialization
        this->SpringArmComponent = this->UObject::CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArmComponent"));
        this->SpringArmComponent->SetupAttachment(this->AActor::GetRootComponent());

        // Tranform
        this->SpringArmComponent->SetRelativeLocation(FVector(0.0f, 0.0f, 34.28f)); // Good starting point; may be changed in BP.
        this->SpringArmComponent->SetRelativeRotation(FRotator(-20.0f, 0.0f, 0.0f));
        this->SpringArmComponent->TargetArmLength = 125.0f;

        // Lag
        this->SpringArmComponent->bEnableCameraRotationLag = true;
        this->SpringArmComponent->CameraRotationLagSpeed = 3.0f;

        // Rotation
        this->SpringArmComponent->bInheritPitch = false; // To prevent the camera from going nuts when you're crashing.
        this->SpringArmComponent->bInheritRoll = false; // To prevent the camera from going nuts when you're crashing.

        // Camera Initialization
        this->CameraComponent = this->UObject::CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
        this->CameraComponent->SetupAttachment(this->SpringArmComponent);

        // Transform
        this->CameraComponent->SetRelativeLocation(FVector(-125.0f, 0.0f, 0.0f));
        this->CameraComponent->SetRelativeRotation(FRotator(10.0f, 0.0f, 0.0f));
    }
}