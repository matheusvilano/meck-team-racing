// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameMode.h"
#include "MeckGameModeBase.generated.h"

/**
 * Base GameMode class for Meck Team Racing.
 */
UCLASS()
class MECKTEAMRACING_API AMeckGameModeBase : public AGameMode
{
	GENERATED_BODY()
	
};
