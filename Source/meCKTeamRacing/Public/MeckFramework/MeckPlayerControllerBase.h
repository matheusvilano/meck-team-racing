// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "Components/MeckInputComponent.h"

#include "MeckPlayerControllerBase.generated.h"

/**
 * Custom player controller for meCK Team Racing.
 */
UCLASS()
class MECKTEAMRACING_API AMeckPlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
	
	#pragma region Initialization

		public:

			// Constructor to set defaults
			AMeckPlayerControllerBase();

		protected:

			// Setup MeckInputComponent and bindings.
			virtual void SetupInputComponent() override;
	
	#pragma endregion

	#pragma region Bindings

		protected:

			// Runs everytime the player controller possesses a pawn. Use to set up input bindings.
			virtual void OnPossess(APawn* InPawn) override;

			// Custom input component with support for binding lambdas.
			UMeckInputComponent* MeckInputComponent = nullptr;

	#pragma endregion
};
