// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "WheeledVehicle.h"

#include "MeckVehiclePawnBase.generated.h"

/**
 * meCK Team Racing's base vehicle class.
 */
UCLASS(Abstract)
class MECKTEAMRACING_API AMeckVehiclePawnBase : public AWheeledVehicle
{
	GENERATED_BODY()

	#pragma region Initialization

		public:

			// Parameterless constructor.
			AMeckVehiclePawnBase();

		protected:

			// Tick function. Runs every frame by default (do not change).
			// virtual void Tick(float DeltaSeconds) override;
			
	#pragma endregion

	#pragma region Camera

		protected:

			// Spring arm component (makes the camera follow the vehicle smoothly).
			UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="Camera")
			USpringArmComponent* SpringArmComponent = nullptr;

			// Chasing camera.
			UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category="Camera")
			UCameraComponent* CameraComponent = nullptr;

	#pragma endregion
};
