// This work by Matheus Vilano is licensed under Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc-nd/4.0/

#pragma once

#include "CoreMinimal.h"
#include <functional>
#include "Components/InputComponent.h"

#include "MeckInputComponent.generated.h"

/**
 * Custom input component that supports binding with lambdas.
 */
UCLASS()
class MECKTEAMRACING_API UMeckInputComponent : public UInputComponent
{
	GENERATED_BODY()
	
	public:

		UMeckInputComponent();

		template<class UserClass>
		FInputActionBinding& BindAction(const FName ActionName, const EInputEvent KeyEvent, UserClass* Object, std::function<void(void)> Function)
		{
			FInputActionBinding InputActionBinding(ActionName, KeyEvent);
			InputActionBinding.ActionDelegate.GetDelegateForManualSet().BindLambda(Function);
			
			return this->UInputComponent::AddActionBinding(InputActionBinding);
		};
};
